const express = require('express');
const fs = require('fs');
const path = require('path');
const bodyParser = require("body-parser");
const request = require("request");
const querystring = require('querystring');
const cors = require('cors');

const app = express();

let allDocuments = [];
let filteredDocuments = [];
let uidToFilter = [];
let documentsToSave = [];

const pathToPublic = path.join(__dirname, "../public");

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(cors());

app.get('/demo-monitor', (req, res) => {
    let currentAuth = false;
    if (typeof req.query.j_auth !== "undefined") {
        currentAuth = req.query.j_auth;
    }

    let url = "http://admin-local.demo.jentis.com/jaql/";

    let preparedRequest = JSON.stringify({
        "ent": "request.return",
        "act": "setjs",
        "arg": {
            "jsonJsLinks": "/public/jentis-plugin-demo-frontend.js"
        },
        "ret": [
            "success"
        ]

    });
    const body = querystring.stringify({"_auth_": currentAuth, "all_projects": preparedRequest});
    const options = {
        uri: url,
        method: "POST",
        headers: {
            "cookie": "XDEBUG_SESSION=XDEBUG_ECLIPSE",
            'Content-Length': body.length,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Origin': '*'
        },
        body: body
    };

    //TODO: Bei Error, muss Array gelöscht werden
    request(options, (err, httpResponse, body) => {
        console.log("");
        if (err) {
            console.log("Couldn't send a request to" + url);
        } //else if (JSON.parse(body)._err_.length !== 0) {
        //     console.log("The server returned the following error: " + JSON.parse(body.substring(5))["_err_"]);
        // }
        //TODO: return HTML as a file instead of text
        res.send('<h2>Demo-Monitor</h2>' +
            '<form >' +
            '<input type="text" name="userId" placeholder="User Id"><br>' +
            '<button class="btn btn-primary">Filter</button>' +
            '</form>' +
            '<table class="table" id="data-table">' +
            "<thead>"+
            "<tr>"+
            "<th scope='col'>Art des Dokuments</th>"+
            "<th scope='col'>Dokument</th>"+
            "</tr>"+
            "</thead>"+
            "<tbody>" +
            "</tbody>"+
            '</table>');
    });

});

app.get('/public/jentis-plugin-demo-frontend.js', (req, res) => {
    //TODO: errorhandling
    let jsFile = fs.readFileSync(pathToPublic + '/jentis-plugin-demo-frontend.js');
    res.send(jsFile);
});

app.get('/manifest', (req, res) => {
    //TODO: errorhandling
    let manifest = fs.readFileSync(pathToPublic + '/manifest.json');
    res.send(manifest);
});

app.get("/sessionid", (req, res) => {
    if (typeof req.query.userId !== "undefined") {
        let currentSessionId = generateSessionId(req.query.userId);
        res.send({sessionId: currentSessionId});
        return;
    }
    res.send({errorMessage: "No UserId specified"});
});

app.get("/document/get", (req, res) => {
    if (typeof req.query.sessionId !== "undefined") {
        filteredDocuments = filterDocuments(req.query.sessionId);
        return res.send(filteredDocuments);
    }
    res.send({"errorMessage": "No documents found!"});
});

app.post("/document/set", (req, res) => {
    documentsToSave = req.body;
    JSON.parse(documentsToSave.document).forEach((value, index) => {
        persistDocument(value);
    });
    documentsToSave = [];
    res.send({"infoMessage": "Saved your documents"});
});

app.listen(3000, () => {
    console.log("Server is running on 3000.")
});

function generateSessionId(requestedUserId) {
    let currentTimestamp = Date.now();
    let sessionId = currentTimestamp + "_" + requestedUserId;
    allDocuments.push(
        {
            "sessionId": sessionId,
            "docUserId": requestedUserId,
            "timestamp": currentTimestamp,
            "docs": []
        }
    );
    uidToFilter.push(requestedUserId);
    return sessionId;
}

function filterDocuments(filterSessionId) {
    let ret = allDocuments.find((document) => {
        return document.sessionId === filterSessionId;
    });
    return ret;
}

function persistDocument(documentsToSave) {
    if (
        (typeof documentsToSave !== "undefined"
            && typeof documentsToSave._id !== "undefined")
        || (typeof documentsToSave !== "undefined"
            && typeof documentsToSave.parent !== "undefined"
            && typeof documentsToSave.parent.userid !== "undefined"
        )
    ) {
        allDocuments.forEach((value, index, array) => {
            if ((value.docUserId === documentsToSave._id)
                || (typeof documentsToSave.parent !== "undefined"
                    && typeof documentsToSave.parent.user !== "undefined"
                    && value.userId === documentsToSave.parent.user)) {
                value.docs.push(documentsToSave);
            }
        });
    }
}

function garbageCollector() {
    let uidHelp = [];
    allDocuments.forEach((value, index, array) => {
        if ((value.timestamp + (60000 * 30)) < Date.now()) {
            uidHelp.push(array.splice(index, 1));
        }
    });
    uidToFilter.forEach((value, index, array) => {
        if (uidHelp.includes(value.docUserId)) {
            array.splice(index, 1);
        }
    })
}

sleep = async function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};

(async () => {
        while (true) {
            garbageCollector();
            await sleep(1000);
        }
    }
)();
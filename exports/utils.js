var exports = module.exports = {};

//++++++++++Logging++++++++++

const { createLogger, format, transports } = require('winston');

const logger = createLogger({
    level: 'info',
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.errors({ stack: true }),
        format.splat(),
        format.json()
    ),
    transports: [

        new transports.Console({
            level: "silly",
            format: format.combine(
                format.colorize(),
                format.simple()
            )
        }),
        new transports.File({ filename: 'error.log', level: 'error' }),
    ]
});


exports.log_handling = function(ex, type, text, value) {
    logger.log({
        level: type,
        message: ex,
        text: text,
        value: value

    });
};

//++++++++++Timer++++++++++

exports.sleep = async function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};

exports.update_doc = function (oldDocument, newDocument) {

    for (let key in newDocument) {

        if (typeof oldDocument[key] === "object" && Array.isArray(oldDocument[key])) {
            oldDocument[key] = oldDocument[key].concat(newDocument[key]);
        } else if (typeof oldDocument[key] === "object" && Array.isArray(oldDocument[key]) === false) {
            Object.assign(oldDocument[key], newDocument[key]);
        } else if (typeof oldDocument[key] === "undefined") {
            oldDocument[key] = newDocument[key];
        } else if (typeof oldDocument[key] !== "undefined") {
            oldDocument[key] = newDocument[key];
        }

        oldDocument.tmspUpdate = newDocument.tmspUpdate;
        //await dbo.collection(newDoc.documentType).update({ _id: oldDoc._id}, oldDoc,{upsert:true});
    }
    return oldDocument;

};


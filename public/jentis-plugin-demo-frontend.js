//TODO: debugging with "jtdebug" cookie

document.addEventListener("DOMContentLoaded", async (event) => {
    let sessionId = false;
    let userId = "123";
    const jentisUrl = "http://admin-local.demo.jentis.com:3000";
    const table = document.getElementById("data-table");
    const userIdForm = document.querySelector("form");
    const userIdFormInput = document.querySelector("input");


    getSessionId = async function (userId) {
        const url = (jentisUrl + "/sessionid?userId=" + userId);
        const myHeaders = ({
            'Access-Control-Allow-Origin': '*'
        });
        const options = {
            headers: myHeaders,
            method: 'GET',
            mode: 'cors'
        };

        const response = await fetch(url, options);
        const data = await response.json();
        if (data.sessionId) {
            return data.sessionId
        }
        return false;
    };

    getDocuments = async function (sessionId) {
        console.log("getDoc happened");
        if (sessionId) {
            const url = (jentisUrl + "/document/get?sessionId=" + sessionId);
            const myHeaders = ({
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: myHeaders,
                method: 'GET',
                mode: 'cors'
            };

            const response = await fetch(url, options);
            const data = await response.json();
            if (data.docs) {
                if (data.docs.length > 0) {
                    updateHtml(data.docs);
                    return data.docs;
                }
                return data.docs;
            }
            return false;
        }
        return;
    };

    updateHtml = function (docs) {
        let rowsInTable = table.rows.length;
        for (let i = 1; i < rowsInTable; i++){
            table.deleteRow(1);
        }
        for (let i = 0; i < docs.length; i++) {
            let row = table.insertRow(i + 1);
            let cell1 = row.insertCell(0);
            let cell2 = row.insertCell(1);
            cell1.innerHTML = docs[i].documentType;
            //TODO: Make JSONs more readable
            cell2.innerHTML = JSON.stringify(docs[i], undefined, 4);
        }
    };


    userIdForm.addEventListener("submit", async (e) => {
        //TODO: Give visual confirmation of the click
        e.preventDefault(); // seite nicht neu laden
        userId = userIdFormInput.value;
        if (!sessionId) {
            sessionId = await getSessionId(userId);
        }
        await getDocuments(sessionId);
    });

    //TODO: continously look for new documents
    //setInterval(await getDocuments(sessionId), 1000);


});
